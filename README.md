# Unity in Flutter

Integrating unity project into flutter as a widget.

## Steps to Follow:

- Step 1: Create unity project and add flutter package in into the unity project.

- Step 2: Create a flutter project add a folder named unity and insert unity project there.

- Step 3: After this run import as flutter package in unity project. This will automatically do the changes required in flutter project.

- Step 4: install flutter project in yaml file.

- Step 5: Now you can use unity project in flutter as a widget
