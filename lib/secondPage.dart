import 'package:flutter_unity_widget/flutter_unity_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class UnityTestingWrapper extends StatefulWidget {
  UnityTestingState createState() => UnityTestingState();
}

class UnityTestingState extends State<UnityTestingWrapper> {
  UnityWidgetController controller;
  double slidervalue = 0.0;

  void initState() {
    super.initState();
  }

  Widget build(context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(title: Text("Flutter int unity test")),
      body: Card(
        margin: const EdgeInsets.all(10),
        clipBehavior: Clip.antiAlias,
        child: Stack(
          children: <Widget>[
            UnityWidget(
              onUnityViewCreated: createdUnityWidget,
            ),
            Positioned(
              bottom: 20,
              left: 20,
              right: 20,
              child: Card(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Text("Rotation Speed"),
                    )
                  ],
                ),
              ),
            ),
            Slider(
              onChanged: (value) {
                setState(() {
                  slidervalue = value;
                });
                setRotationSpeed(value.toString());
              },
              value: slidervalue,
              min: 0,
              max: 30,
            )
          ],
        ),
      ),
    ));
  }

  void createdUnityWidget(c) {
    this.controller = c;
  }

  void setRotationSpeed(String speed) {
    controller.postMessage('Cube', 'SetRotationSpeed', speed);
  }
}
